package rs.agile.tdd;

import org.junit.jupiter.api.*;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName(value = "Calc test")
@TestInstance(value = TestInstance.Lifecycle.PER_CLASS)
public class TestCalc{
    public Calc calc;

    @BeforeAll
    public void init(){
        calc = new Calc();
    }

    @Test
    public void testAdd(){
        assertEquals(new BigInteger("2"), calc.add(new BigInteger("1"), new BigInteger("1")));
        assertEquals(new BigInteger("5"), calc.add(new BigInteger("2"),new BigInteger("3")));
        assertEquals(new BigInteger("-6"), calc.add(new BigInteger("-9"), new BigInteger("3")));
        assertEquals(new BigInteger("10000000"), calc.add(new BigInteger("5000000"), new BigInteger("5000000")));
        assertEquals(new BigInteger("10000000000000"), calc.add(new BigInteger("5000000000000"), new BigInteger("5000000000000")));
    }
}