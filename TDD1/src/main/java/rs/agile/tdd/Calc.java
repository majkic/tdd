package rs.agile.tdd;

import java.math.BigInteger;

public class Calc {

    public BigInteger add(BigInteger a, BigInteger b) {
        BigInteger add = a.add(b);
        return add;
    }
}
